import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class TrieTest {
	private static BufferedReader inFile;

	public static void main(String[] args) throws Exception {
		Node tree = Trie.newTrieTree();

		System.out.println("Processing text file...");
		try {
			inFile = new BufferedReader(new FileReader("assets/usernames.txt"));
		} catch (FileNotFoundException f) {
			inFile = new BufferedReader(new FileReader("usernames.txt"));
		}
		long time = System.nanoTime();
		String word = null;
		while ((word = inFile.readLine()) != null) {
			Trie.insertWord(tree, word);
		}
		System.out.println("processing finished. time: " + (System.nanoTime() - time) + "ns");
		if (inFile != null) {
			inFile.close();
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String username = null;
			System.out.print("enter username to search: ");
			username = in.readLine();
			if (username.equals("memory")) {
				DecimalFormat formatter = new DecimalFormat("#,###.##");
				System.out.println(formatter.format((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + " MB.");
			} else if (username.equals("add")) {
				System.out.print("Enter a username to add (this will only be added to the current session. The username file will not be modified.) :");
				String usernameToAdd = in.readLine();
				Trie.insertWord(tree, usernameToAdd);
				System.out.println("username: " + usernameToAdd + " added.");
			} else {
				long runtime = System.nanoTime();
				int substringIndex = username.indexOf("@");
				if (substringIndex > 0) {
					username = username.substring(0, substringIndex);
				}
				username = username.replaceAll("\\d*$", "").toLowerCase().replace(" ", "");
				if (!Trie.find(tree, username)) {
					System.out.println("username " + username + " is available.");
				} else {
					int index = 1;
					String newWord = username + index;
					while (Trie.find(tree, newWord)) {
						newWord = username + index++;
					}
					System.out.println("username " + newWord + " is available.");
				}
				System.out.println("total search time: " + (System.nanoTime() - runtime) + " ns");
			}
		}

	}
}
