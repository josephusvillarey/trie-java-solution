
public class Trie {
	private static final int offset = 97;
	private static final int numOffset = 74;

	public static Node newTrieTree() {
		return (new Node('\0', false));
	}

	private static int getOffsetIndex(char c) {
		int offsetIndex = c - offset;
		if (offsetIndex < 0) {
			offsetIndex += numOffset;
		}
		return offsetIndex;
	}

	public static void insertWord(Node root, String word) {
		int length = word.length();
		char[] letters = word.toCharArray();
		Node curNode = root;
		for (int i = 0; i < length; i++) {
			if (curNode.children[getOffsetIndex(letters[i])] == null) {
				curNode.children[getOffsetIndex(letters[i])] = new Node(letters[i], i == length - 1 ? true : false);
			}
			curNode = curNode.children[getOffsetIndex(letters[i])];
			if (i == length - 1) {
				curNode.setFullWord(true);
			}
		}
	}

	public static boolean find(Node root, String word) {
		char[] letters = word.toCharArray();
		int length = letters.length;
		Node curNode = root;
		int i;
		for (i = 0; i < length; i++) {
			if (curNode == null) {
				return false;
			}
			curNode = curNode.children[getOffsetIndex(letters[i])];
		}
		if (i == length && curNode == null) {
			return false;
		}
		if (curNode != null && !curNode.isFullWord) {
			return false;
		}
		return true;
	}
}