public class Node {
	char letter;
	Node[] children;
	boolean isFullWord;

	Node(char letter, boolean isFullWord) {
		this.letter = letter;
		children = new Node[36];
		this.isFullWord = isFullWord;
	}
	
	public void setFullWord(boolean isFullWord) {
		this.isFullWord = isFullWord;
	}
}