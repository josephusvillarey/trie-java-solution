**Instructions:**

Import as java project in eclipse, then run as Java Application. 
Main class is TrieTest if no configuration is generated.

**Reserved Words**
You can enter these reserved words from the prompt to trigger the function.

memory - prints memory usage of the application.

add - switches to "add username" mode. after a username is added, the program will revert back to default mode (recommend username).